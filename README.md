# Backend position test

technical test for backend developer position


## Important


- Within the project, all the methods used are documented, describing their functionality.
- The project has an integrated database with h2, so a connection to an external database is not necessary.
- Scripts are integrated within the project that generate the database and some records necessary for its basic operation.
```bash
  file: src/main/resources/import.sql
```
- The postman collection comes inside the resources folder.
```bash
  file: src/main/resources/collection/test_fullstack.postman_collection.json
```

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/ArmandoPL/backend-test.git
```

Go to the project directory

```bash
  cd backend-test
```

Install dependencies

```bash
  mvn eclipse:eclipse
  mvn eclipse:clean
```

Start the server

```bash
  mvn spring-boot:run
```

## Features

Tests
- Create test
```http
  POST  pi/test/create-test
```
- Get created test
```http
  GET   /api/test/get-tests
```
- Assign test
```http
  POST  /api/test-assignation/assign
```
- Apply test
```http
  POST  pi/test-assignation/apply
```
- Get test results
```http
  GET   /api/test-assignation/get-results?assignation=${id_assignation}
```
| Parameter    | Type          | Description                                |
| :--------    | :-------      | :--------------------------------          |
| `assignation`| `query_param` | **Required**. Id of assignation to consult |

Students
- Create Student
```http
  POST  /api/student/create-student
```
- Get created students
```http
  GET   /api/student/get-students
```

TimeZone
- I included a request to be able to consult the catalog of time zones
```http
  GET	 http://worldtimeapi.org/api/timezone
```

## Feedback

If you have any feedback, please reach out to me at armandoperalta648@gmail.com


## Authors

- [@ArmandoPL](https://gitlab.com/ArmandoPL)


##  Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/armando-peralta-2138a81a0)

