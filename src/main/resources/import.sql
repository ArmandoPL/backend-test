insert into students(id, age, city, name, time_zone) values (default, 24, 'Madrid Espana', 'Armando Peralta', 'America/Mexico_City');
insert into students(id, age, city, name, time_zone) values (default, 18, 'Buenos Aires', 'Benito', 'America/Argentina/Buenos_Aires');
insert into students(id, age, city, name, time_zone) values (default, 35, 'Tokio', 'Paco Perez', 'Asia/Tokyo');
insert into students(id, age, city, name, time_zone) values (default, 16, 'California', 'Don Cheto', 'America/Los_Angeles');
insert into students(id, age, city, name, time_zone) values (default, 20, 'Berlin', 'Batman', 'Europe/Berlin');
insert into students(id, age, city, name, time_zone) values (default, 24, 'Madrid Espana', 'Armando Peralta', 'Europe/Madrid');
insert into students(id, age, city, name, time_zone) values (default, 18, 'Buenos Aires', 'Benito', 'America/Argentina/Buenos_Aires');
insert into students(id, age, city, name, time_zone) values (default, 35, 'Tokio', 'Paco Perez', 'Asia/Tokyo');
insert into students(id, age, city, name, time_zone) values (default, 16, 'California', 'Don Cheto', 'America/Los_Angeles');
insert into students(id, age, city, name, time_zone) values (default, 20, 'Berlin', 'Batman', 'Europe/Berlin');
insert into tests (id, create_at) values (default, now());

insert into questions (id, percentage, question) values (default, 10.0, '¿En dónde se originaron los juegos olímpicos?');
insert into answers (id, answer, correct) values (default, 'Italia- Roma', false) ;
insert into answers (id, answer, correct) values (default, 'Grecia-Esparta', false) ;
insert into answers (id, answer, correct) values (default, 'Grecia-Roma', false) ;
insert into answers (id, answer, correct) values (default, 'Grecia-Olimpia ', true) ;

insert into questions (id, percentage, question) values (default, 10.0, '¿Cuál de los siguientes monumentos es característico de la cultura egipcia? ');
insert into answers (id, answer, correct) values (default, 'La Estatua de la Libertad. ', false);
insert into answers (id, answer, correct) values (default, 'Las Pirámides de Giza. ', true);
insert into answers (id, answer, correct) values (default, 'El Coliseo Romano. ', false);
insert into answers (id, answer, correct) values (default, 'El Faro de Alejandría ', false);

insert into questions (id, percentage, question) values (default, 10.0, '¿En qué país de Europa se encuentra la Torre de Pisa? ');
insert into answers (id, answer, correct) values (default, 'Italia.', true)
insert into answers (id, answer, correct) values (default, 'Alemania.', false)
insert into answers (id, answer, correct) values (default, 'Rusia.', false)
insert into answers (id, answer, correct) values (default, 'Croacia.', false)

insert into questions (id, percentage, question) values (default, 10.0, 'La capital de Suiza es: ')
insert into answers (id, answer, correct) values (default, 'Berlín. ', false)
insert into answers (id, answer, correct) values (default, 'Berna. ', false)
insert into answers (id, answer, correct) values (default, 'Dushanb ', true)
insert into answers (id, answer, correct) values (default, 'Ciudad Vieja Zuisa ', false)

insert into questions (id, percentage, question) values (default, 10.0, '¿Quién pintó “La Última Cena”?');
insert into answers (id, answer, correct) values (default, 'Leonardo Dicaprio ', false)
insert into answers (id, answer, correct) values (default, 'Leonardo da Vinci ', true)
insert into answers (id, answer, correct) values (default, 'Angelino Medoro ', false)
insert into answers (id, answer, correct) values (default, 'Anhy Medoro ', false)

insert into questions (id, percentage, question) values (default, 10.0, 'El autor de la novela “Cinco Esquinas” es el escritor…');
insert into answers (id, answer, correct) values (default, 'Mario Vargas Llosa. ', true)
insert into answers (id, answer, correct) values (default, 'Abraham Valdelomar. ', false)
insert into answers (id, answer, correct) values (default, 'José Eduardo Eielson. ', false)
insert into answers (id, answer, correct) values (default, 'Julio Ramón Ribeyro. ', false)

insert into questions (id, percentage, question) values (default, 10.0, '¿Cuál de las siguientes culturas no es originaria de América?');
insert into answers (id, answer, correct) values (default, 'La cultura egipcia. ', true)
insert into answers (id, answer, correct) values (default, 'La cultura inca. ', false)
insert into answers (id, answer, correct) values (default, 'La cultura maya. ', false)
insert into answers (id, answer, correct) values (default, 'La cultura Chimú.', false)

insert into questions (id, percentage, question) values (default, 10.0, '¿Dónde se encuentran las cataratas del Niagara?');
insert into answers (id, answer, correct) values (default, 'México y Canadá ', false)
insert into answers (id, answer, correct) values (default, 'Argentina, Brasil y Paraguay. ', false)
insert into answers (id, answer, correct) values (default, 'Groenlandia y Canadá. ', false)
insert into answers (id, answer, correct) values (default, 'EE. UU y Canadá ', true)

insert into questions (id, percentage, question) values (default, 10.0, '¿Entre qué países fue la guerra de los 6 días?');
insert into answers (id, answer, correct) values (default, 'Bolivia, Argentina y Paraguay ', false)
insert into answers (id, answer, correct) values (default, 'Israel, Egipto, Jordania, Irak y Siria ', true)
insert into answers (id, answer, correct) values (default, 'Perú, España y Chile. ', false)
insert into answers (id, answer, correct) values (default, 'Francia, Estados Unidos, Turquía y Rusia.  ', false)

insert into questions (id, percentage, question) values (default, 10.0, '¿Quién fue el primer hombre en revelarse del yugo español durante el Virreinato, pidiendo la libertad de Hispanoamérica y la eliminación de toda forma de explotación indígena?');
insert into answers (id, answer, correct) values (default, 'Mariano Melgar. ', false)
insert into answers (id, answer, correct) values (default, 'José Olaya ', false)
insert into answers (id, answer, correct) values (default, 'Mateo Pumacahua ', false)
insert into answers (id, answer, correct) values (default, 'José Gabriel Condorcanqui ', true)


insert into tests_questions (testdo_id, questions_id) values (1, 1);
insert into tests_questions (testdo_id, questions_id) values (1, 2);
insert into tests_questions (testdo_id, questions_id) values (1, 3);
insert into tests_questions (testdo_id, questions_id) values (1, 4);
insert into tests_questions (testdo_id, questions_id) values (1, 5);
insert into tests_questions (testdo_id, questions_id) values (1, 6);
insert into tests_questions (testdo_id, questions_id) values (1, 7);
insert into tests_questions (testdo_id, questions_id) values (1, 8);
insert into tests_questions (testdo_id, questions_id) values (1, 9);
insert into tests_questions (testdo_id, questions_id) values (1, 10);

insert into questions_answers (questiondo_id, answers_id) values (1, 1);
insert into questions_answers (questiondo_id, answers_id) values (1, 2);
insert into questions_answers (questiondo_id, answers_id) values (1, 3);
insert into questions_answers (questiondo_id, answers_id) values (1, 4);

insert into questions_answers (questiondo_id, answers_id) values (2, 5);
insert into questions_answers (questiondo_id, answers_id) values (2, 6);
insert into questions_answers (questiondo_id, answers_id) values (2, 7);
insert into questions_answers (questiondo_id, answers_id) values (2, 8);

insert into questions_answers (questiondo_id, answers_id) values (3, 9);
insert into questions_answers (questiondo_id, answers_id) values (3, 10);
insert into questions_answers (questiondo_id, answers_id) values (3, 11);
insert into questions_answers (questiondo_id, answers_id) values (3, 12);

insert into questions_answers (questiondo_id, answers_id) values (4, 13);
insert into questions_answers (questiondo_id, answers_id) values (4, 14);
insert into questions_answers (questiondo_id, answers_id) values (4, 15);
insert into questions_answers (questiondo_id, answers_id) values (4, 16);

insert into questions_answers (questiondo_id, answers_id) values (5, 17);
insert into questions_answers (questiondo_id, answers_id) values (5, 18);
insert into questions_answers (questiondo_id, answers_id) values (5, 19);
insert into questions_answers (questiondo_id, answers_id) values (5, 20);

insert into questions_answers (questiondo_id, answers_id) values (6, 21);
insert into questions_answers (questiondo_id, answers_id) values (6, 22);
insert into questions_answers (questiondo_id, answers_id) values (6, 23);
insert into questions_answers (questiondo_id, answers_id) values (6, 24);

insert into questions_answers (questiondo_id, answers_id) values (7, 25);
insert into questions_answers (questiondo_id, answers_id) values (7, 26);
insert into questions_answers (questiondo_id, answers_id) values (7, 27);
insert into questions_answers (questiondo_id, answers_id) values (7, 28);

insert into questions_answers (questiondo_id, answers_id) values (8, 29);
insert into questions_answers (questiondo_id, answers_id) values (8, 30);
insert into questions_answers (questiondo_id, answers_id) values (8, 31);
insert into questions_answers (questiondo_id, answers_id) values (8, 32);

insert into questions_answers (questiondo_id, answers_id) values (9, 33);
insert into questions_answers (questiondo_id, answers_id) values (9, 34);
insert into questions_answers (questiondo_id, answers_id) values (9, 35);
insert into questions_answers (questiondo_id, answers_id) values (9, 36);

insert into questions_answers (questiondo_id, answers_id) values (10, 37);
insert into questions_answers (questiondo_id, answers_id) values (10, 38);
insert into questions_answers (questiondo_id, answers_id) values (10, 39);
insert into questions_answers (questiondo_id, answers_id) values (10, 40);

insert into test_assignation (id, local_date, student_id, student_zone_date, test_id, presented) values (default, '2022-09-14 16:03:36', 6, ' 2022-09-14 23:03:36', 1, false);


insert into test_application (id, assignation_id, student_percentage) values (default, 1, 6)

insert into student_answer (id, question_id, student_answer) values (default, 1, 2)

insert into student_answer (id, question_id, student_answer) values (default, 2, 6)

insert into student_answer (id, question_id, student_answer) values (default, 3, 11)

insert into student_answer (id, question_id, student_answer) values (default, 4, 15)

insert into student_answer (id, question_id, student_answer) values (default, 5, 18)

insert into student_answer (id, question_id, student_answer) values (default, 6, 23)

insert into student_answer (id, question_id, student_answer) values (default, 7, 25)

insert into student_answer (id, question_id, student_answer) values (default, 8, 30)

insert into student_answer (id, question_id, student_answer) values (default, 9, 36)

insert into student_answer (id, question_id, student_answer) values (default, 10, 40)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 1)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 2)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 3)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 4)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 5)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 6)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 7)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 8)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 9)

insert into test_application_student_answer (test_applicationdo_id, student_answer_id) values (1, 10)

update test_assignation set presented=true where id=1
