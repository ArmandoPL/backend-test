package com.dbi.springboot.app.test.fullstack.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

public class TestAssignationDTO implements Serializable {

    private static final long serialVersionUID = -4889314548884986106L;

    private Long studentId;
    private Long testId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date testDate;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }
}
