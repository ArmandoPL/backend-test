/**
 * @author Adriana Galvan
 * @date 26/08/2019
 * @company Nativelabs 2019
 * @file ServiceException.java
 */

package com.dbi.springboot.app.test.fullstack.util;

public class ServiceException extends Exception {
	
	/**
	 * Default Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del Error
	 */
	private Integer idError;
	
	private String messageException;
	
	/**
	 * Identificador de fila con error
	 */
	private Integer numRow;
	
	/**
	 * Identificador de fila con error
	 */
	private Integer numColumn;

	/**
	 * Constructor Base
	 */
	public ServiceException() {
	
	}
	
	/**
	 * Constructor que recibe como parametro el
	 * mensaje de la Excepcion
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
	}
	
	/**
	 * Constructor que recibe parametro message
	 */
	public ServiceException(Throwable throwable) {
		super(throwable);	
	}
	
	/**
	 * Constructor
	 * @param throwable
	 * @param noError
	 */
	public ServiceException(Integer idError , Throwable throwable) {
		super(throwable);
		this.idError = idError;
		
	}
	
	/**
	 * Constructor
	 * @param throwable
	 * @param noError
	 */
	public ServiceException(Integer idError , Exception exception) {
		super(exception);
		this.idError = idError;
		
	}
	
	/**
	 * Constructor
	 * @param throwable
	 * @param noError
	 */
	public ServiceException(Integer idError) {
		this.idError = idError;
		
	}
	
	/**
	 * Constructor
	 * @param idError the idError to set and Description
	 */
	public ServiceException(Integer idError, String messageException) {
		super(messageException);
		this.idError = idError;
		this.messageException = messageException;
	}
	
	/**
	 * Constructor
	 * @param idError the idError to set and Description
	 */
	public ServiceException(Integer idError, Integer numRow) {
		//super(messageException);
		this.numRow = numRow;
		this.idError = idError;
	}
	
	/**throws
	 * Constructor
	 * @param idError the idError to set and Description
	 */
	public ServiceException(Integer idError, Integer numRow, Integer numColumn) {
		//super(messageException);
		this.numRow = numRow;
		this.idError = idError;
	}
	
	
	/**
	 * @return the idError
	 */
	public Integer getIdError() {
		return idError;
	}

	/**
	 * @param idError the idError to set
	 */
	public void setIdError(Integer idError) {
		this.idError = idError;
	}
	


	public String getMessageException() {
		return messageException;
	}

	public void setMessageException(String messageException) {
		this.messageException = messageException;
	}

	/**
	 * @return the numRow
	 */
	public Integer getNumRow() {
		return numRow;
	}

	/**
	 * @param numRow the numRow to set
	 */
	public void setNumRow(Integer numRow) {
		this.numRow = numRow;
	}

	public Integer getNumColumn() {
		return numColumn;
	}

	public void setNumColumn(Integer numColumn) {
		this.numColumn = numColumn;
	}
	
	
	
}
