package com.dbi.springboot.app.test.fullstack.controller;

import com.dbi.springboot.app.test.fullstack.model.dto.BaseResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationApplyDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationDTO;
import com.dbi.springboot.app.test.fullstack.service.inter.TestAssignationService;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.constant.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
public class TestAssignationController {
    @Autowired
    private TestAssignationService testAssignationService;

    @PostMapping("/api/test-assignation/assign")
    public ResponseEntity<BaseResponseDTO> assignTest(@RequestBody TestAssignationDTO testAssignation) {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            baseResponseDTO.setCode(Constant.CREATED);
            baseResponseDTO.setMessage("Test assigned successfully");
            baseResponseDTO.setResponseData(testAssignationService.assignTest(testAssignation));
        } catch (ServiceException e) {
            baseResponseDTO.setCode(e.getIdError());
            baseResponseDTO.setMessage(e.getMessage());
        } catch (ParseException e) {
            baseResponseDTO.setCode(500);
            baseResponseDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.CREATED);
    }

    @PostMapping("/api/test-assignation/apply")
    public ResponseEntity<BaseResponseDTO> apply(@RequestBody TestAssignationApplyDTO testAssignationApply) {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            baseResponseDTO.setCode(Constant.CREATED);
            baseResponseDTO.setMessage("Test applied successfully");
            baseResponseDTO.setResponseData(testAssignationService.applyTest(testAssignationApply));
        } catch (ServiceException e) {
            baseResponseDTO.setCode(e.getIdError());
            baseResponseDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.CREATED);
    }

    @GetMapping("/api/test-assignation/get-results")
    public ResponseEntity<BaseResponseDTO> getResults(@RequestParam(name = "assignation") Long assignationId) {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            baseResponseDTO.setCode(Constant.SUCCESS);
            baseResponseDTO.setMessage("Results obtained successfully");
            baseResponseDTO.setResponseData(testAssignationService.getResults(assignationId));
        } catch (ServiceException e) {
            baseResponseDTO.setCode(e.getIdError());
            baseResponseDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);
    }
}
