package com.dbi.springboot.app.test.fullstack.util.constant;

public interface Constant {

    Double PERCENTAGE_QUESTIONS = 100.0;

    Integer ANSWERS_VALIDATOR = 0;

    Integer BAD_REQUEST = 400;

    Integer CREATED = 201;

    Integer SUCCESS = 200;

    Integer NOT_FOUNT = 404;
}
