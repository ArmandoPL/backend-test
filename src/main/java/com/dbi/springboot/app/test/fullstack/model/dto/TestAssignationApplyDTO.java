package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;
import java.util.List;

public class TestAssignationApplyDTO implements Serializable {

    private static final long serialVersionUID = 2392211902249024438L;
    private Long assignationId;
    private List<AssignationAnswersDTO> studentAnswers;

    public Long getAssignationId() {
        return assignationId;
    }

    public void setAssignationId(Long assignationId) {
        this.assignationId = assignationId;
    }

    public List<AssignationAnswersDTO> getStudentAnswers() {
        return studentAnswers;
    }

    public void setStudentAnswers(List<AssignationAnswersDTO> studentAnswers) {
        this.studentAnswers = studentAnswers;
    }
}
