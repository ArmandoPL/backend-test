package com.dbi.springboot.app.test.fullstack.service.impl;

import com.dbi.springboot.app.test.fullstack.model.entity.QuestionDO;
import com.dbi.springboot.app.test.fullstack.model.entity.TestDO;
import com.dbi.springboot.app.test.fullstack.repository.TestRepository;
import com.dbi.springboot.app.test.fullstack.service.inter.TestService;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.constant.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TestServiceImpl implements TestService {
    private final Logger logger = LoggerFactory.getLogger(TestServiceImpl.class);

    @Autowired
    private TestRepository testRepository;


    @Override
    public List<TestDO> getTests() throws ServiceException {
        return ((List<TestDO>) testRepository.findAll());
    }


    @Override
    public TestDO createTest(TestDO test) throws ServiceException {
        validatePercentage(test.getQuestions());
        validateAnswers(test.getQuestions());
        return testRepository.save(test);
    }

    @Override
    public Optional<TestDO> getTestById(Long testId) {
        return testRepository.findById(testId);
    }


    /**
     * Se valida que la suma del valor de las preguntas sume 100
     *
     * @param questions Arreglo de preguntas
     * @throws ServiceException Control de errores
     */
    private void validatePercentage(List<QuestionDO> questions) throws ServiceException {
        Double percentage = 0.0;
        percentage = questions.stream().mapToDouble(question -> question.getPercentage()).sum();
        logger.info(Double.valueOf(String.valueOf(percentage)) + "");
        if (Double.valueOf(String.valueOf(percentage)) != 100.0) {
            logger.info("no suma 100");
            throw new ServiceException(Constant.BAD_REQUEST, "The percentages of the questions do not add up to 100");
        }
    }

    /**
     * Se valida que las respuestas tengan 4 respuestas y una opcion correcta
     *
     * @param questions Objeto de respuestas de una pregunta
     * @throws ServiceException Control de errores
     */
    private void validateAnswers(List<QuestionDO> questions) throws ServiceException {
        List<QuestionDO> questionLessOrMoreAnswers = questions.stream().filter(question -> question.getAnswers().size() != 4).collect(Collectors.toList());
        if (questionLessOrMoreAnswers.size() > Constant.ANSWERS_VALIDATOR) {
            throw new ServiceException(Constant.BAD_REQUEST, "All questions must have 4 answers");
        }

        List<QuestionDO> questionsIncludesWrongAnswers = questions.stream().filter(question -> question.getAnswers().stream().filter(answer -> answer.getCorrect()).collect(Collectors.toList()).size() != 1).collect(Collectors.toList());
        if (questionsIncludesWrongAnswers.size() > Constant.ANSWERS_VALIDATOR) {
            throw new ServiceException(Constant.BAD_REQUEST, "Answers must have one correct option.");
        }
    }


}
