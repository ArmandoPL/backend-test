package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;

public class BaseResponseDTO implements Serializable {

    private static final long serialVersionUID = 3514039705512933660L;

    private Integer code;

    private String message;

    private Object responseData;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }
}

