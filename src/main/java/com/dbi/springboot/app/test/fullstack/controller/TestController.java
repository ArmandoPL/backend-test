package com.dbi.springboot.app.test.fullstack.controller;

import com.dbi.springboot.app.test.fullstack.model.dto.BaseResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.entity.TestDO;
import com.dbi.springboot.app.test.fullstack.service.inter.TestService;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.constant.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {
    @Autowired
    private TestService testService;

    @GetMapping("/api/test/get-tests")
    public  ResponseEntity<BaseResponseDTO> getTests() {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            baseResponseDTO.setCode(Constant.SUCCESS);
            baseResponseDTO.setMessage("Tests List");
            baseResponseDTO.setResponseData(testService.getTests());
        } catch (ServiceException e) {
            baseResponseDTO.setCode(e.getIdError());
            baseResponseDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);

    }

    @PostMapping("/api/test/create-test")
    public ResponseEntity<BaseResponseDTO> createTest(@RequestBody TestDO test) {
        BaseResponseDTO baseResponseDO = new BaseResponseDTO();
        try {
            baseResponseDO.setCode(Constant.CREATED);
            baseResponseDO.setMessage("Test created successfully");
            baseResponseDO.setResponseData(testService.createTest(test));
        } catch (ServiceException e) {
            baseResponseDO.setCode(e.getIdError());
            baseResponseDO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDO, HttpStatus.CREATED);
    }

}
