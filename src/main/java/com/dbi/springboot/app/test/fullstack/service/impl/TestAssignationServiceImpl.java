package com.dbi.springboot.app.test.fullstack.service.impl;

import com.dbi.springboot.app.test.fullstack.model.dto.QuestionsResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestApplicationResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationApplyDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationDTO;
import com.dbi.springboot.app.test.fullstack.model.entity.*;
import com.dbi.springboot.app.test.fullstack.repository.TestApplicationRepository;
import com.dbi.springboot.app.test.fullstack.repository.TestAssignationRepository;
import com.dbi.springboot.app.test.fullstack.service.inter.StudentService;
import com.dbi.springboot.app.test.fullstack.service.inter.TestAssignationService;
import com.dbi.springboot.app.test.fullstack.service.inter.TestService;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import com.dbi.springboot.app.test.fullstack.util.constant.Constant;
import com.dbi.springboot.app.test.fullstack.util.mapper.TestAssignationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TestAssignationServiceImpl implements TestAssignationService {
    private final Logger logger = LoggerFactory.getLogger(TestAssignationServiceImpl.class);

    @Autowired
    TestAssignationRepository assignationRepository;

    @Autowired
    TestApplicationRepository testApplicationRepository;

    @Autowired
    StudentService studentService;

    @Autowired
    TestService testService;

    @Override
    public TestAssignationDTO assignTest(TestAssignationDTO testAssignation) throws ServiceException, ParseException {
        StudentDO studentDO = getStudentData(testAssignation.getStudentId());
        Date studentTimeZoneDate = getStudentTimeZoneDate(testAssignation.getTestDate(), studentDO.getTimeZone());
        TestAssignationDO assignationDO = TestAssignationMapper.mapper(testAssignation, studentDO, studentTimeZoneDate);
        assignationRepository.save(assignationDO);
        return testAssignation;
    }

    @Override
    public TestApplicationResponseDTO applyTest(TestAssignationApplyDTO testAssignationApply) throws ServiceException {
        TestAssignationDO assignationDO = getAssignationById(testAssignationApply.getAssignationId());
        validateTestPresetation(assignationDO);
        TestDO testDO = getTestData(assignationDO.getTestId());
        Double studentPercentage = calculateStudentPercentage(testAssignationApply, testDO);
        TestApplicationDO testApplicationDO = TestAssignationMapper.mapApplication(testAssignationApply, studentPercentage);
        testApplicationDO = testApplicationRepository.save(testApplicationDO);
        TestApplicationResponseDTO testApplicationResponse = getStudentReponse(testApplicationDO, testDO, testAssignationApply);
        assignationDO.setPresented(true);
        assignationRepository.save(assignationDO);
        return testApplicationResponse;
    }

    @Override
    public TestApplicationResponseDTO getResults(Long assignationId) throws ServiceException {
        Optional<TestApplicationDO> testApplicationDO = testApplicationRepository.findByAssignationId(assignationId);
        if (!testApplicationDO.isPresent())
            throw new ServiceException(Constant.NOT_FOUNT, "the test has not been presented");
        TestAssignationDO assignationDO = getAssignationById(assignationId);
        TestDO testDO = getTestData(assignationDO.getTestId());
        TestApplicationResponseDTO testApplicationResponse = new TestApplicationResponseDTO();
        testApplicationResponse.setStudentPercentage(testApplicationDO.get().getStudentPercentage());
        testApplicationResponse.setNote(testApplicationDO.get().getStudentPercentage() >= 60.0 ? "APPROVED" : "NOT APPROVED");
        testApplicationResponse.setQuestions(getRightAnswersResults(testDO, testApplicationDO.get().getStudentAnswer()));
        return testApplicationResponse;
    }

    /**
     * Se obtiene el objeto de resultados de un examen previamente presentado
     *
     * @param testDO        Objeto del examen presentado
     * @param studentAnswer Objeto de las respuestas del estudiante
     * @return Objeto de los resultados de un examen
     */
    private List<QuestionsResponseDTO> getRightAnswersResults(TestDO testDO, List<StudentAnswerDO> studentAnswer) {
        List<QuestionsResponseDTO> questionsResponseDTOS;
        questionsResponseDTOS = studentAnswer.stream().map(studentAnswers -> {
            QuestionsResponseDTO questionsResponseDTO = new QuestionsResponseDTO();
            QuestionDO questionDO = testDO.getQuestions().stream().filter(question -> question.getId() == studentAnswers.getQuestionId()).findFirst().get();
            questionsResponseDTO.setQuestion(questionDO.getQuestion());
            questionsResponseDTO.setStudentAnswer(questionDO.getAnswers().stream().filter(answer -> answer.getId() == studentAnswers.getStudentAnswer()).findFirst().get().getAnswer());
            questionsResponseDTO.setRightAnswer(questionDO.getAnswers().stream().filter(answerDO -> answerDO.getCorrect()).findFirst().get().getAnswer());
            return questionsResponseDTO;
        }).collect(Collectors.toList());
        return questionsResponseDTOS;

    }

    /**
     * Se realiza la validacion de si el examen ya ha sido presentado
     *
     * @param assignationDO Objeto de la asignacion
     * @throws ServiceException Control de errores
     */
    private void validateTestPresetation(TestAssignationDO assignationDO) throws ServiceException {
        if (assignationDO.getPresented()) {
            throw new ServiceException(Constant.BAD_REQUEST, "the test has already been applied");
        }
    }

    /**
     * Se obtiene response para el estudiante
     *
     * @param testApplicationDO    El objeto de la aplicacion del estudianto
     * @param testDO
     * @param testAssignationApply
     * @return Devuelve el response para el estudiante
     */
    private TestApplicationResponseDTO getStudentReponse(TestApplicationDO testApplicationDO, TestDO testDO, TestAssignationApplyDTO testAssignationApply) {
        TestApplicationResponseDTO testApplicationResponse = new TestApplicationResponseDTO();
        testApplicationResponse.setStudentPercentage(testApplicationDO.getStudentPercentage());
        testApplicationResponse.setNote(testApplicationDO.getStudentPercentage() >= 60.0 ? "APPROVED" : "NOT APPROVED");
        testApplicationResponse.setQuestions(getRightAnswers(testDO, testAssignationApply));
        return testApplicationResponse;
    }

    /**
     * Se obtiene arreglo de las preguntas para saber cuales son las respuestas correctas del examen presentado
     *
     * @param testDO
     * @param testAssignationApply
     * @return
     */
    private List<QuestionsResponseDTO> getRightAnswers(TestDO testDO, TestAssignationApplyDTO testAssignationApply) {
        List<QuestionsResponseDTO> questionsResponseDTOS;
        questionsResponseDTOS = testAssignationApply.getStudentAnswers().stream().map(studentAnswers -> {
            QuestionsResponseDTO questionsResponseDTO = new QuestionsResponseDTO();
            QuestionDO questionDO = testDO.getQuestions().stream().filter(question -> question.getId() == studentAnswers.getQuestionId()).findFirst().get();
            questionsResponseDTO.setQuestion(questionDO.getQuestion());
            questionsResponseDTO.setStudentAnswer(questionDO.getAnswers().stream().filter(answer -> answer.getId() == studentAnswers.getStudentAnswer()).findFirst().get().getAnswer());
            questionsResponseDTO.setRightAnswer(questionDO.getAnswers().stream().filter(answerDO -> answerDO.getCorrect()).findFirst().get().getAnswer());
            return questionsResponseDTO;
        }).collect(Collectors.toList());
        return questionsResponseDTOS;
    }

    /**
     * Se comparan las respuestas del estudiante con las respuestas de las preguntas y se obtiene una evaluacion
     *
     * @param testAssignationApply Preguntas del estudiante
     * @param testDO               Preguntas del examen
     * @return Calificacion total obtenida
     */
    private Double calculateStudentPercentage(TestAssignationApplyDTO testAssignationApply, TestDO testDO) {
        return testAssignationApply.getStudentAnswers().stream().mapToDouble(studentAnswers -> {
            QuestionDO questionDO = testDO.getQuestions().stream().filter(question -> question.getId() == studentAnswers.getQuestionId()).findFirst().get();
            AnswerDO answerDO = questionDO.getAnswers().stream().filter(answer -> answer.getCorrect()).findFirst().get();
            Double percentageSum = 0.0;
            if (answerDO.getId() == studentAnswers.getStudentAnswer()) {
                percentageSum += questionDO.getPercentage();
            }
            return percentageSum;
        }).sum();
    }

    /**
     * Se obtiene la informacion de la aplicacion del examen mediante su id
     *
     * @param assignationId Id de la asignacion
     * @return Devuelve los datos de la asignacion del examen
     * @throws ServiceException Control de errores
     */
    private TestAssignationDO getAssignationById(Long assignationId) throws ServiceException {
        Optional<TestAssignationDO> assignationDO = assignationRepository.findById(assignationId);
        if (assignationDO.isPresent()) {
            return assignationDO.get();
        } else {
            throw new ServiceException(Constant.NOT_FOUNT, "The assignation doesn't exist");
        }
    }

    /**
     * Se obtiene la hora y fecha en la zona horaria a la que el estudiante pertenece
     *
     * @param testDate Fecha local que se presenta el examen
     * @param timeZone Zona horaria del estudiante
     * @return devuelve la fecha y hora en la zona horaria del estudiante
     * @throws ParseException Control de errores
     */
    private Date getStudentTimeZoneDate(Date testDate, String timeZone) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(testDate);
        ZonedDateTime dateTimaZone = ZonedDateTime.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY) + 5, calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), 00, ZoneId.of("America/Bogota"));
        Date convertedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(String.valueOf(dateTimaZone.withZoneSameInstant(ZoneId.of(timeZone))).replace("[Europe/Madrid]", "").replace("T", " "));
        return convertedDate;
    }

    /**
     * Se obtiene la informacion de un estudiante y se valida si relamente existe
     *
     * @param studentId Id del estudiante a buscar
     * @return Objeto del estudiante encontrado
     * @throws ServiceException Control de errores
     */
    private StudentDO getStudentData(Long studentId) throws ServiceException {
        Optional<StudentDO> student = studentService.getStudentById(studentId);
        if (student.isPresent()) {
            return student.get();
        } else {
            throw new ServiceException(Constant.NOT_FOUNT, "The student doesn't exist");
        }
    }

    /**
     * Se obtiene la informacion de un  examen y se valida si realmente existe
     *
     * @param testId Id del examen a buscar
     * @return Objeto del examen encontrado
     * @throws ServiceException Control de errores
     */
    private TestDO getTestData(Long testId) throws ServiceException {
        Optional<TestDO> testDO = testService.getTestById(testId);
        if (testDO.isPresent()) {
            return testDO.get();
        } else {
            throw new ServiceException(Constant.NOT_FOUNT, "The test doesn't exist");
        }
    }
}
