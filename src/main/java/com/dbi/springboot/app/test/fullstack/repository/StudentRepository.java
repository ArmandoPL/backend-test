package com.dbi.springboot.app.test.fullstack.repository;

import com.dbi.springboot.app.test.fullstack.model.entity.StudentDO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentDO, Long> {
}
