package com.dbi.springboot.app.test.fullstack.service.impl;

import com.dbi.springboot.app.test.fullstack.model.entity.StudentDO;
import com.dbi.springboot.app.test.fullstack.repository.StudentRepository;
import com.dbi.springboot.app.test.fullstack.service.inter.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<StudentDO> getStudents() {
        return (List<StudentDO>) studentRepository.findAll();
    }

    @Override
    public StudentDO createStudent(StudentDO student) {
        return studentRepository.save(student);
    }

    @Override
    public Optional<StudentDO> getStudentById(Long studentId) {
        return studentRepository.findById(studentId);
    }
}
