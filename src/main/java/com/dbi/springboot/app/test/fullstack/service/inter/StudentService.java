package com.dbi.springboot.app.test.fullstack.service.inter;

import com.dbi.springboot.app.test.fullstack.model.entity.StudentDO;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    /**
     * Se realiza consulta de los estudiantes registrados
     * @return Lista de los estudiantes obtenidos
     */
    List<StudentDO> getStudents() throws ServiceException;

    /**
     * Se realza el registro de un estudiante
     *
     * @param student Datos a guardar del estudiante
     * @return Objeto del estudiante creado
     */
    StudentDO createStudent(StudentDO student) throws ServiceException;

    /**
     * Se buscan los datos de un estudiando mediante su id
     *
     * @param studentId Id del estudiante
     * @return Objeto con los datos del estudiante
     */
    Optional<StudentDO> getStudentById(Long studentId);
}
