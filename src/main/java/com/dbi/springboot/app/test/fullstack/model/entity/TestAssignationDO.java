package com.dbi.springboot.app.test.fullstack.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "testAssignation")
public class TestAssignationDO implements Serializable {

    private static final long serialVersionUID = 876261203677674043L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "students", nullable = false)
    private Long studentId;

    @JoinColumn(name = "tests", nullable = false)
    private Long testId;

    private Date localDate;

    private Boolean presented = false;

    private Date studentZoneDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public Date getLocalDate() {
        return localDate;
    }

    public void setLocalDate(Date localDate) {
        this.localDate = localDate;
    }

    public Date getStudentZoneDate() {
        return studentZoneDate;
    }

    public void setStudentZoneDate(Date studentZoneDate) {
        this.studentZoneDate = studentZoneDate;
    }

    public Boolean getPresented() {
        return presented;
    }

    public void setPresented(Boolean presented) {
        this.presented = presented;
    }
}
