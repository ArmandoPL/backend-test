package com.dbi.springboot.app.test.fullstack.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "studentAnswer")
public class StudentAnswerDO implements Serializable {

    private static final long serialVersionUID = -6503377216329457118L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long questionId;


    private Long studentAnswer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(Long studentAnswer) {
        this.studentAnswer = studentAnswer;
    }
}
