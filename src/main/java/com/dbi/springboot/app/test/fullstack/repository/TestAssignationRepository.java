package com.dbi.springboot.app.test.fullstack.repository;

import com.dbi.springboot.app.test.fullstack.model.entity.TestAssignationDO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestAssignationRepository extends CrudRepository<TestAssignationDO, Long> {
}
