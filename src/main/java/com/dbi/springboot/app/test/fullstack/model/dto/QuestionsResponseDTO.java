package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;
import java.util.List;

public class QuestionsResponseDTO implements Serializable {

    private static final long serialVersionUID = 2383769224429284182L;

    private String question;

    private String studentAnswer;

    private String rightAnswer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        this.studentAnswer = studentAnswer;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public void setRightAnswer(String rightAnswer) {
        this.rightAnswer = rightAnswer;
    }
}
