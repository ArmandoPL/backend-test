package com.dbi.springboot.app.test.fullstack.service.inter;

import com.dbi.springboot.app.test.fullstack.model.entity.TestDO;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;

import java.util.Optional;

public interface TestService {

    /**
     * Se realiza consulta de los examenes registrados
     *
     * @return Lista de examenes encontrados
     * @throws ServiceException Control de errores
     */
    Iterable<TestDO> getTests() throws ServiceException;

    /**
     * Se realiza guardado de examenes
     *
     * @param test Recibe objeto del examen a guardar
     * @return Devuelve el examen ya guardado
     * @throws ServiceException Control de errores
     */
    TestDO createTest(TestDO test) throws ServiceException;

    /**
     * Se consulta el detalle de un examen mediante su id
     *
     * @param testId Id del examen a buscar
     * @return Objeto del examen consultado
     */
    Optional<TestDO> getTestById(Long testId);
}
