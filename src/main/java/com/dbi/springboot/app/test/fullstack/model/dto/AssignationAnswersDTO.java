package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;

public class AssignationAnswersDTO implements Serializable {

    private static final long serialVersionUID = 8145462335323189712L;
    private Long questionId;
    private Long studentAnswer;

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(Long studentAnswer) {
        this.studentAnswer = studentAnswer;
    }
}
