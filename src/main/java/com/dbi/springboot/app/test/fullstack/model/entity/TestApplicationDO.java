package com.dbi.springboot.app.test.fullstack.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "testApplication")
public class TestApplicationDO implements Serializable {

    private static final long serialVersionUID = 778724067412404078L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long assignationId;

    private Double studentPercentage;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    private List<StudentAnswerDO> studentAnswer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAssignationId() {
        return assignationId;
    }

    public void setAssignationId(Long assignationId) {
        this.assignationId = assignationId;
    }

    public Double getStudentPercentage() {
        return studentPercentage;
    }

    public void setStudentPercentage(Double studentPercentage) {
        this.studentPercentage = studentPercentage;
    }

    public List<StudentAnswerDO> getStudentAnswer() {
        return studentAnswer;
    }

    public void setStudentAnswer(List<StudentAnswerDO> studentAnswer) {
        this.studentAnswer = studentAnswer;
    }
}
