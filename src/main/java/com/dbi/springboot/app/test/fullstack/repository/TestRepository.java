package com.dbi.springboot.app.test.fullstack.repository;

import com.dbi.springboot.app.test.fullstack.model.entity.TestDO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends CrudRepository<TestDO, Long> {
}
