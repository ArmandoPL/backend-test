package com.dbi.springboot.app.test.fullstack.util.mapper;

import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationApplyDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationDTO;
import com.dbi.springboot.app.test.fullstack.model.entity.StudentAnswerDO;
import com.dbi.springboot.app.test.fullstack.model.entity.StudentDO;
import com.dbi.springboot.app.test.fullstack.model.entity.TestApplicationDO;
import com.dbi.springboot.app.test.fullstack.model.entity.TestAssignationDO;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TestAssignationMapper {

    public static TestAssignationDO mapper(TestAssignationDTO testAssignation, StudentDO studentDO, Date studentTimeZoneDate) {
        TestAssignationDO assignationDO = new TestAssignationDO();
        assignationDO.setStudentId(studentDO.getId());
        assignationDO.setTestId(testAssignation.getTestId());
        assignationDO.setLocalDate(testAssignation.getTestDate());
        assignationDO.setStudentZoneDate(studentTimeZoneDate);
        return assignationDO;
    }

    public static TestApplicationDO mapApplication(TestAssignationApplyDTO testAssignationApply, Double studentPercentage) {
        TestApplicationDO testApplicationDO = new TestApplicationDO();
        testApplicationDO.setAssignationId(testAssignationApply.getAssignationId());
        testApplicationDO.setStudentPercentage(studentPercentage);

        List<StudentAnswerDO> studentAnswerDOs = testAssignationApply.getStudentAnswers().stream().map(assignationAnswersDTO -> {
            StudentAnswerDO studentAnswerDO = new StudentAnswerDO();
            studentAnswerDO.setStudentAnswer(assignationAnswersDTO.getStudentAnswer());
            studentAnswerDO.setQuestionId(assignationAnswersDTO.getQuestionId());
            return studentAnswerDO;
        }).collect(Collectors.toList());

        testApplicationDO.setStudentAnswer(studentAnswerDOs);
        return testApplicationDO;
    }
}
