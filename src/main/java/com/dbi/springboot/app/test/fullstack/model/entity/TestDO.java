package com.dbi.springboot.app.test.fullstack.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tests")
public class TestDO implements Serializable {
    private static final long serialVersionUID = 1053957931546084221L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST,CascadeType.MERGE})
//    @JoinTable(name = "questions",  joinColumns = @JoinColumn(name = "test_id"), inverseJoinColumns = @JoinColumn(name = "question_id"), uniqueConstraints = {@UniqueConstraint(columnNames = {"test_id", "question_id"})})
    private List<QuestionDO> questions;

    @Column(name = "create_at")
    @Temporal(TemporalType.DATE)
    private Date createAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<QuestionDO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDO> questions) {
        this.questions = questions;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}
