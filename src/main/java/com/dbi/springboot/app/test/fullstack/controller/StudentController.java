package com.dbi.springboot.app.test.fullstack.controller;

import com.dbi.springboot.app.test.fullstack.model.dto.BaseResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.entity.StudentDO;
import com.dbi.springboot.app.test.fullstack.service.inter.StudentService;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/api/student/get-students")
    public ResponseEntity<BaseResponseDTO> getStudents() {
        BaseResponseDTO baseResponseDTO = new BaseResponseDTO();
        try {
            baseResponseDTO.setCode(201);
            baseResponseDTO.setMessage("Consult user");
            baseResponseDTO.setResponseData(studentService.getStudents());
        } catch (ServiceException e) {
            baseResponseDTO.setCode(e.getIdError());
            baseResponseDTO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDTO, HttpStatus.OK);
    }

    @PostMapping("/api/student/create-student")
    public ResponseEntity<BaseResponseDTO> createStudent(@RequestBody StudentDO student) {
        BaseResponseDTO baseResponseDO = new BaseResponseDTO();
        try {
            baseResponseDO.setCode(201);
            baseResponseDO.setMessage("Create user");
            baseResponseDO.setResponseData(studentService.createStudent(student));
        } catch (ServiceException e) {
            baseResponseDO.setCode(e.getIdError());
            baseResponseDO.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(baseResponseDO, HttpStatus.CREATED);
    }
}
