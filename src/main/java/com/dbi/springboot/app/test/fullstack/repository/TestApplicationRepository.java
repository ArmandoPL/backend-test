package com.dbi.springboot.app.test.fullstack.repository;

import com.dbi.springboot.app.test.fullstack.model.entity.TestApplicationDO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestApplicationRepository extends CrudRepository<TestApplicationDO, Long> {

    /**
     * Se consulta los datos de una aplicacion de examen mediante el id de la asignacion
     * @param assignationId Id de la asignacion
     * @return Objeto de la aplicacion del examen
     */
    Optional<TestApplicationDO> findByAssignationId(Long assignationId);
}
