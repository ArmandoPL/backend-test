package com.dbi.springboot.app.test.fullstack.model.dto;

import java.io.Serializable;
import java.util.List;

public class TestApplicationResponseDTO implements Serializable {

    private static final long serialVersionUID = -487360856512856334L;

    private Double studentPercentage;
    private String note;

    private List<QuestionsResponseDTO> questions;

    public Double getStudentPercentage() {
        return studentPercentage;
    }

    public void setStudentPercentage(Double studentPercentage) {
        this.studentPercentage = studentPercentage;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<QuestionsResponseDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionsResponseDTO> questions) {
        this.questions = questions;
    }
}
