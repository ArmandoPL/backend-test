package com.dbi.springboot.app.test.fullstack.service.inter;

import com.dbi.springboot.app.test.fullstack.model.dto.TestApplicationResponseDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationApplyDTO;
import com.dbi.springboot.app.test.fullstack.model.dto.TestAssignationDTO;
import com.dbi.springboot.app.test.fullstack.util.ServiceException;

import java.text.ParseException;

public interface TestAssignationService {

    /**
     * Se realiza la asignacion de un examen a un estudiante
     * @param testAssignation Datos de la asignacion del examan
     * @return Objeto del registro de la asignacion
     * @throws ServiceException Control de errores
     */
    TestAssignationDTO assignTest(TestAssignationDTO testAssignation) throws ServiceException, ParseException;

    /**
     * Se realiza el guardado de las respuestas del usuario y se obtiene su calificacion
     *
     * @param testAssignationApply
     * @return
     */
    TestApplicationResponseDTO applyTest(TestAssignationApplyDTO testAssignationApply) throws ServiceException;

    /**
     * Se obtienen los resultados obtenido de un estudiante en un examen
     * @param assignationId Id de la prueba a consultar
     * @return Resultados del examen
     * @throws ServiceException Control de errores
     */
    TestApplicationResponseDTO getResults(Long assignationId) throws ServiceException;
}
